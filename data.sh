#!/bin/bash

set -e

echo `pwd`

cic_data_dir=${CIC_DATA_DIR:-/tmp/cic}

export PGPASSWORD=$DATABASE_PASSWORD

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_cache
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_cache

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_eth
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_eth

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_ussd
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_ussd

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_notify
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_notify

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_meta
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_meta

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_signer
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_signer

/usr/bin/dropdb -e --if-exists -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_meta
/usr/bin/createdb -e -U $DATABASE_USER -h $DATABASE_HOST -p $DATABASE_PORT cic_meta

mkdir -vp /tmp/cic/pgp
cp -vR testdata/pgp/* /tmp/cic/pgp/

set +e
