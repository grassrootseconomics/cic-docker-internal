#!/bin/bash

set -e

. _forced.sh

# cic-meta 0.0.6
cic_meta_commit=3c3867cd6e636bcf662084a0e6ebc1a5fe67ffc1
#cic_meta_url=https://gitlab.com/grassrootseconomics/cic-meta.git/
cic_meta_url=file:///home/lash/src/ext/cic/grassrootseconomics/cic-meta

wd=`pwd`
d=`mktemp -d`
pushd $d

if [ "$force_rebuild" -eq 0 ]; then
	img_cic_meta=`docker images -q grassrootseconomics:cic-meta-server`
fi

if [ -z $img_cic_meta ]; then
	git clone --depth 1 $cic_meta_url cic-meta
	pushd cic-meta
	git fetch --depth 1 origin $cic_meta_commit
	git checkout $cic_meta_commit
	docker build -t grassrootseconomics:cic-meta -f docker/Dockerfile .
	docker build -t grassrootseconomics:cic-meta-server -f docker/Dockerfile .
	popd
else
	>&2 echo "cic-meta exists: $img_cic_meta"
fi

popd

set +e
