#!/bin/bash

keystore_file=$(realpath ./keystore/UTC--2021-01-08T17-18-44.521011372Z--eb3907ecad74a0013c259d5874ae7f22dcbcc95c)

cic_data_dir=${CIC_DATA_DIR:-/tmp/cic}
if [ -f $cic_data_dir/.deploy ]; then
	>&2 echo Data directory $cic_data_dir already contains a previous deployment.
	exit 1
fi
mkdir -vp $cic_data_dir

d=`mktemp -d`

cat .env_config_template > $d/.env_template
cat .env_dockercompose_template >> $d/.env_template

pushd $d

set -a
#. .env_template
set +a
echo "environment:"
printenv

set -e

# This is a temporary solution for building the Bancor contracts using the bancor protocol repository truffle setup
# It should be built as an earlier provision within the docker base image instead
pushd /usr/src

#DEV_ETH_RESERVE_ADDRESS=`giftable-token-deploy -p $ETH_PROVIDER -o $DEV_ETH_ACCOUNT_RESERVE_OWNER -m $DEV_ETH_ACCOUNT_RESERVE_MINTER $DEV_ETH_RESERVE_AMOUNT`
DEV_ETH_RESERVE_ADDRESS=`giftable-token-deploy -p $ETH_PROVIDER -y $keystore_file -i $CIC_CHAIN_SPEC --minter $DEV_ETH_ACCOUNT_RESERVE_MINTER --minter $DEV_ETH_ACCOUNT_CONTRACT_DEPLOYER -v -w --name "Sarafu" --symbol "SRF" $DEV_ETH_RESERVE_AMOUNT`

#BANCOR_REGISTRY_ADDRESS=`cic-bancor-deploy --bancor-dir /usr/local/share/cic/bancor -z $DEV_ETH_RESERVE_ADDRESS -p $ETH_PROVIDER -o $DEV_ETH_ACCOUNT_CONTRACT_DEPLOYER`

CIC_ACCOUNTS_INDEX_ADDRESS=`eth-accounts-index-deploy -i $CIC_CHAIN_SPEC -p $ETH_PROVIDER -y $keystore_file --writer $DEV_ETH_ACCOUNT_ACCOUNTS_INDEX_WRITER -vv -w`

CIC_REGISTRY_ADDRESS=`cic-registry-deploy -i $CIC_CHAIN_SPEC -y $keystore_file -k CICRegistry -k BancorRegistry -k AccountRegistry -k TokenRegistry -k AddressDeclarator -k Faucet -k TransferApproval -p $ETH_PROVIDER -vv -w`
cic-registry-set -y $keystore_file -r $CIC_REGISTRY_ADDRESS -i $CIC_CHAIN_SPEC -k CICRegistry  -p $ETH_PROVIDER $CIC_REGISTRY_ADDRESS -vv
#cic-registry-set -r $CIC_REGISTRY_ADDRESS -i $CIC_CHAIN_SPEC -k BancorRegistry -p $ETH_PROVIDER $BANCOR_REGISTRY_ADDRESS -vv
cic-registry-set -y $keystore_file -r $CIC_REGISTRY_ADDRESS -i $CIC_CHAIN_SPEC -k AccountRegistry -p $ETH_PROVIDER $CIC_ACCOUNTS_INDEX_ADDRESS  -vv

# Deploy address declarator registry
#>&2 echo "deploy address declarator contract"
declarator_description=0x546869732069732074686520434943206e6574776f726b000000000000000000
CIC_DECLARATOR_ADDRESS=`eth-address-declarator-deploy -y $keystore_file -i $CIC_CHAIN_SPEC -p $ETH_PROVIDER -w -v $declarator_description`
#echo CIC_DECLARATOR_ADDRESS=$CIC_DECLARATOR_ADDRESS
#export CIC_DECLARATOR_ADDRESS=$CIC_DECLARATOR_ADDRESS
popd

# skipped for now pending refactor
#BANCOR_REGISTRY_ADDRESS=$BANCOR_REGISTRY_ADDRESS
cat << EOF > .env_deploy
DEV_ETH_RESERVE_ADDRESS=$DEV_ETH_RESERVE_ADDRESS
DEV_ETH_RESERVE_AMOUNT=$DEV_ETH_RESERVE_AMOUNT
CIC_ACCOUNTS_INDEX_ADDRESS=$CIC_ACCOUNTS_INDEX_ADDRESS
DEV_MNEMONIC="$DEV_MNEMONIC"
CIC_REGISTRY_ADDRESS=$CIC_REGISTRY_ADDRESS
CIC_DECLARATOR_ADDRESS=$CIC_DECLARATOR_ADDRESS
CIC_TRUSTED_ADDRESSES=$DEV_ETH_ACCOUNT_CONTRACT_DEPLOYER
EOF

cp -v .env_template $cic_data_dir/.env
cat .env_deploy >> $cic_data_dir/.env

echo -n $CIC_REGISTRY_ADDRESS > $cic_data_dir/cic_registry_address.txt
echo -n $DEV_ETH_RESERVE_ADDRESS > $cic_data_dir/reserve_address.txt
#echo -n $BANCOR_REGISTRY_ADDRESS > $cic_data_dir/bancor_registry_address.txt
echo -n $CIC_ACCOUNTS_INDEX_ADDRESS > $cic_data_dir/accounts_index_address.txt
echo -n $DEV_MNEMONIC > $cic_data_dir/mnemonic.txt

touch $cic_data_dir/.deploy

set +e
