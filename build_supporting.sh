#!/bin/bash

set -e

. _forced.sh

# cic-cache 0.1.3
cic_cache_commit=76c89cfe08bbb029147e14bbfa6aed79d0c8e473
cic_cache_url=https://gitlab.com/grassrootseconomics/cic-cache.git/

d=`mktemp -d`
pushd $d

if [ "$force_rebuild" -eq 0 ]; then
	img_cic_cache=`docker images -q grassrootseconomics:cic-cache-service`
fi

if [ -z $img_cic_cache ]; then
	git clone --depth 1 $cic_cache_url cic-cache
	pushd cic-cache
	git fetch --depth 1 origin $cic_cache_commit
	git checkout $cic_cache_commit
	docker build -t grassrootseconomics:cic-cache -f docker/Dockerfile .
	docker build -t grassrootseconomics:cic-cache-service -f docker/Dockerfile.service .
	docker build -t grassrootseconomics:cic-cache-uwsgi -f docker/Dockerfile.uwsgi .
	popd
else
	>&2 echo "cic-cache exists: $img_cic_cache"
fi

popd

set +e
