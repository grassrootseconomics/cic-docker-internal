force_rebuild=0
while test $# != 0; do
	case "$1" in
		-f)
			force_rebuild=1
			shift
			;;
		*)
			>&2 echo "unknown arg $1"
			shift
			;;
	esac
done
