version: "3"
services:

  eth:
    image: grassrootseconomics:bloxberg-dev
    hostname: eth
    volumes:
      - ./bloxberg_config:/home/parity/.local/share/io.parity.ethereum
    restart: unless-stopped
    entrypoint: /start.sh
    user: "0:0"
    ports:
      - ${HTTP_PORT_ETH:-8545}:8545
      - "8546:8546"
      - "30303:30303"

  postgres:
    image: postgres:12.5-alpine
    container_name: cic_postgres
    environment:
      POSTGRES_PASSWORD: $DATABASE_PASSWORD
      PGDATA: /tmp/cic/postgres
    ports:
      - ${HTTP_PORT_POSTGRES}:5432
    volumes:
      - ${LOCAL_VOLUME_DIR:-/tmp/cic}/postgres:/tmp/cic/postgres

  redis:
    image: redis:6.0.9-alpine
    ports:
      - ${HTTP_PORT_REDIS}:6379
    command: "--loglevel verbose"


  bee:
    image: ethersphere/bee:0.4.1
    container_name: bee
    environment:
      BEE_NETWORK_ID: ${BEE_NETWORK_ID:-313}
      BEE_PASSWORD: $BEE_PASSWORD
    ports:
      - 1633:1633
      - 1635:1635
    command: "start --swap-enable=false --standalone"
    volumes:
      - ${LOCAL_VOLUME_DIR:-/tmp/cic}/bee:/tmp/cic/bee

  cic-cache-tracker:
    image: grassrootseconomics:cic-cache-service
    environment:
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      ETH_PROVIDER: $ETH_PROVIDER
      BANCOR_DIR: $BANCOR_DIR
      CIC_TRUST_ADDRESS: $DEV_ETH_ACCOUNT_CONTRACT_DEPLOYER
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_CACHE
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      ETH_ABI_DIR: $ETH_ABI_DIR
      PGPASSWORD: $DATABASE_PASSWORD
    deploy:
      restart_policy:
        condition: on-failure
    depends_on:
      - postgres
      - eth
    command: "/root/start_tracker.sh"
    #entrypoint: ["/usr/local/bin/cic-cache-tracker", "-vv"]
    #command: "/usr/local/bin/cic-cache-tracker -vv"

  cic-cache-server:
    image: grassrootseconomics:cic-cache-uwsgi
    environment:
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_CACHE
      PGPASSWORD: $DATABASE_PASSWORD
      SERVER_PORT: 80
    ports:
      - ${HTTP_PORT_CIC_CACHE}:80
    depends_on:
      - postgres
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_uwsgi.sh"

  cic-eth-tasker:
    image: grassrootseconomics:cic-eth-service
    environment:
      ETH_PROVIDER: $ETH_PROVIDER
      ETH_ABI_DIR: $ETH_ABI_DIR
      ETH_GAS_PROVIDER_ADDRESS: $DEV_ETH_ACCOUNT_GAS_PROVIDER
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      PGPASSWORD: $DATABASE_PASSWORD
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
      SIGNER_SOCKET_PATH: $SIGNER_SOCKET_PATH
      SIGNER_SECRET: $SIGNER_SECRET
      ETH_ACCOUNT_ACCOUNTS_INDEX_WRITER: $DEV_ETH_ACCOUNT_ACCOUNTS_INDEX_WRITER
      TASKS_TRACE_QUEUE_STATUS: $TASKS_TRACE_QUEUE_STATUS
    depends_on:
      - eth
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    volumes:
      - ${LOCAL_VOLUME_DIR:-/tmp/cic}/signer:/tmp/cic/signer
    command: "/root/start_tasker.sh -q cic-eth -vv --trace-queue-status"

  cic-eth-manager-head:
    image: grassrootseconomics:cic-eth-service
    environment:
      ETH_PROVIDER: $ETH_PROVIDER
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
      TASKS_TRANSFER_CALLBACKS: $TASKS_TRANSFER_CALLBACKS
    depends_on:
      - eth
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_manager.sh head -vv"

  cic-eth-manager-history:
    image: grassrootseconomics:cic-eth-service
    environment:
      ETH_PROVIDER: $ETH_PROVIDER
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
    depends_on:
      - eth
      - postgres
      - redis
        #deploy:
      #restart_policy:
      #  condition: on-failure
    command: "/root/start_manager.sh history -vv"

  cic-eth-dispatcher:
    image: grassrootseconomics:cic-eth-service
    environment:
      ETH_PROVIDER: $ETH_PROVIDER
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
    depends_on:
      - eth
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_dispatcher.sh -q cic-eth -vv"


  cic-eth-retrier:
    image: grassrootseconomics:cic-eth-service
    environment:
      ETH_PROVIDER: $ETH_PROVIDER
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
      CIC_TX_RETRY_DELAY: $CIC_TX_RETRY_DELAY
      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
    depends_on:
      - eth
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_retry.sh -q cic-eth -vv"


  cic-eth-server:
    image: grassrootseconomics:cic-eth-service
    environment:
#      ETH_PROVIDER: $ETH_PROVIDER
#      DATABASE_USER: $DATABASE_USER
#      DATABASE_HOST: $DATABASE_HOST
#      DATABASE_PORT: $DATABASE_PORT
#      DATABASE_PASSWORD: $DATABASE_PASSWORD
#      DATABASE_NAME: $DATABASE_NAME_CIC_ETH
#      DATABASE_ENGINE: $DATABASE_ENGINE
#      DATABASE_DRIVER: $DATABASE_DRIVER
#      CIC_REGISTRY_ADDRESS: $CIC_REGISTRY_ADDRESS
      CIC_CHAIN_SPEC: $CIC_CHAIN_SPEC
#      BANCOR_DIR: $BANCOR_DIR
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
      SERVER_PORT: 80
    ports:
      - ${HTTP_PORT_CIC_ETH}:80
    depends_on:
#      - eth
#      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
          #command: "/bin/bash-c uwsgi --wsgi-file /usr/src/cic-eth/cic_eth/runnable/server_agent.py --http :80 --pyargv '-vv'"
          #command: "uwsgi"
    entrypoint:
      - "/usr/local/bin/uwsgi"
      - "--wsgi-file"
      - "/usr/src/cic-eth/cic_eth/runnable/server_agent.py"
      - "--http"
      - ":80"
    command: "--pyargv -vv"



  cic-notify-tasker:
    image: grassrootseconomics:cic-notify-service
    environment:
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_NOTIFY
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      PGPASSWORD: $DATABASE_PASSWORD
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
      TASKS_AFRICASTALKING: $TASKS_AFRICASTALKING
      TASKS_SMS_DB: $TASKS_SMS_DB
      TASKS_LOG: $TASKS_LOG
    depends_on:
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_tasker.sh -q cic-notify"


  cic-meta-server:
    image: grassrootseconomics:cic-meta-server
    environment:
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_META
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      DATABASE_SCHEMA_SQL_PATH: $DATABASE_SCHEMA_SQL_PATH_CIC_META
      SERVER_PORT: 80
      PGP_PASSPHRASE: $PGP_PASSPHRASE
      PGP_EXPORTS_DIR: $PGP_EXPORTS_DIR
      PGP_PRIVATEKEY_FILE: $PGP_PRIVATEKEY_FILE
    ports:
      - ${HTTP_PORT_CIC_META}:80
    depends_on:
      - postgres
    deploy:
      restart_policy:
        condition: on-failure
    volumes:
      - ${LOCAL_VOLUME_DIR:-/tmp/cic}/pgp:/tmp/cic/pgp
    command: "/root/start_server.sh -vv"

  cic-ussd-server:
    image: grassrootseconomics:cic-ussd
    environment:
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_USSD
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      SERVER_PORT: 80
    ports:
      - ${HTTP_PORT_CIC_USSD}:80
    depends_on:
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_uwsgi.sh"

  cic-ussd-tasker:
    image: grassrootseconomics:cic-ussd
    environment:
      DATABASE_USER: $DATABASE_USER
      DATABASE_HOST: $DATABASE_HOST
      DATABASE_PORT: $DATABASE_PORT
      DATABASE_PASSWORD: $DATABASE_PASSWORD
      DATABASE_NAME: $DATABASE_NAME_CIC_USSD
      DATABASE_ENGINE: $DATABASE_ENGINE
      DATABASE_DRIVER: $DATABASE_DRIVER
      CELERY_BROKER_URL: $CELERY_BROKER_URL
      CELERY_RESULT_URL: $CELERY_RESULT_URL
    depends_on:
      - postgres
      - redis
    deploy:
      restart_policy:
        condition: on-failure
    command: "/root/start_tasker.sh -q cic-ussd"
