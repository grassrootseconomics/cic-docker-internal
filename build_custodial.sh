#!/bin/bash

set -e

. _forced.sh

# cic-eth 0.10.0
cic_eth_commit=cd9e518c16d71d06023fde011fa57457abd9256e
cic_eth_url=https://gitlab.com/grassrootseconomics/cic-eth.git/

# cic-notify 0.3.2
cic_notify_commit=3364792bf934bce1744c1a1261676e89f1404121
cic_notify_url=https://gitlab.com/grassrootseconomics/cic-notify.git/

# cic-ussd==0.3.0-alpha.1.build.2a929cca
cic_ussd_commit=2a929ccab62ddadf2ace01e4dbe2dffe8c1907ad
cic_ussd_url=https://gitlab.com/grassrootseconomics/cic-ussd.git

d=`mktemp -d`
pushd $d

if [ "$force_rebuild" -eq 0 ]; then
	#img_cic_eth=`docker images -q grassrootseconomics:cic-eth-service`
	img_cic_notify=`docker images -q grassrootseconomics:cic-notify-service`
	img_cic_ussd=`docker images -q grassrootseconomics:cic-ussd`
fi

if [ -z $img_cic_eth ]; then
	git clone --depth 1 $cic_eth_url cic-eth
	pushd cic-eth
	git fetch --depth 1 origin $cic_eth_commit
	git checkout $cic_eth_commit
	docker build --no-cache -t grassrootseconomics:cic-eth --build-arg pip_extra_index_url="--index https://pypi.org/simple --extra-index-url https://pip.grassrootseconomics.net:8433" -f docker/Dockerfile .
	docker build -t grassrootseconomics:cic-eth-service --build-arg pip_extra_index_url="--index https://pypi.org/simple --extra-index-url https://pip.grassrootseconomics.net:8433" -f docker/Dockerfile.service .
	popd
else
	>&2 echo "cic-eth exists: $img_cic_eth"
fi

if [ -z $img_cic_notify ]; then
	git clone --depth 1 $cic_notify_url cic-notify
	pushd cic-notify
	git fetch --depth 1 origin $cic_notify_commit
	git checkout $cic_notify_commit
	docker build -t grassrootseconomics:cic-notify-service -f docker/Dockerfile .
#	docker build -t grassrootseconomics:cic-notify-service -f docker/Dockerfile.service .
	popd
else
	>&2 echo "cic-notify exists: $img_cic_notify"
fi

if [ -z $img_cic_ussd ]; then
	git clone --depth 1 $cic_ussd_url cic-ussd
	pushd cic-ussd
	git fetch --depth 1 origin $cic_ussd_commit
	git checkout $cic_ussd_commit
	docker build -t grassrootseconomics:cic-ussd --build-arg pip_extra_index_url="--index https://pypi.org/simple --extra-index-url https://pip.grassrootseconomics.net:8433" -f docker/Dockerfile .
	popd
else
	>&2 echo "cic-ussd exists: $img_cic_ussd"
fi

popd

set +e
