# standard imports
import os
import logging
import argparse
import re
import json
import signal
import random
import time
import uuid
import queue
import threading

# third-party imports
import confini
import web3
import websocket
from cic_registry.chain import ChainSpec
from cic_registry.chain import ChainRegistry
from cic_registry import CICRegistry
from eth_token_index import TokenUniqueSymbolIndex as TokenIndex
from eth_accounts_index import AccountRegistry

from cic_eth.api import Api


logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()
logging.getLogger('websockets.protocol').setLevel(logging.CRITICAL)
logging.getLogger('web3.RequestManager').setLevel(logging.CRITICAL)
logging.getLogger('web3.providers.WebsocketProvider').setLevel(logging.CRITICAL)
logging.getLogger('web3.providers.HTTPProvider').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)

default_data_dir = '/usr/local/share/cic/solidity/abi'

argparser = argparse.ArgumentParser()
argparser.add_argument('-c', type=str, default='./config', help='config file')
argparser.add_argument('-i', '--chain-spec', dest='i', type=str, help='chain spec')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('--abi-dir', dest='abi_dir', type=str, default=default_data_dir, help='Directory containing bytecode and abi (default: {})'.format(default_data_dir))
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be more verbose')
argparser.add_argument('--wait-max', dest='wait_max', default=2.0, type=float, help='maximum time in decimal seconds to wait between transactions')
argparser.add_argument('--batch', default=0, type=int, help='number of events to generate simultaneously')
argparser.add_argument('--account-index-address', dest='account_index', type=str, help='Contract address of accounts index')
argparser.add_argument('--token-index-address', dest='token_index', type=str, help='Contract address of token index')
argparser.add_argument('--approval-escrow-address', dest='approval_escrow', type=str, help='Contract address for transfer approvals')
argparser.add_argument('--declarator-address', dest='declarator', type=str, help='Address of declarations contract to perform lookup against')
argparser.add_argument('-a', '--accounts-index-writer', dest='a', type=str, help='Address of account with access to add to accounts index')

args = argparser.parse_args()

if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)

config = confini.Config(args.c, args.env_prefix)
config.process()
args_override = {
        'ETH_ABI_DIR': getattr(args, 'abi_dir'),
        'CIC_CHAIN_SPEC': getattr(args, 'i'),
        'DEV_ETH_ACCOUNTS_INDEX_ADDRESS': getattr(args, 'account_index'),
        'DEV_ETH_ACCOUNT_ACCOUNTS_INDEX_WRITER': getattr(args, 'a'),
        'DEV_ETH_ERC20_APPROVAL_ESCROW_ADDRESS': getattr(args, 'approval_escrow'),
        'DEV_ETH_TOKEN_INDEX_ADDRESS': getattr(args, 'token_index'),
        }
config.dict_override(args_override, 'cli flag')
config.validate()
config.censor('PASSWORD', 'DATABASE')
config.censor('PASSWORD', 'SSL')
logg.debug('config:\n{}'.format(config))

batchsize = args.batch
if batchsize < 1:
    batchsize = 1
logg.info('batch size {}'.format(batchsize))

re_websocket = r'^wss?:'
re_http = r'^https?:'
blockchain_provider = None
if re.match(re_websocket, config.get('ETH_PROVIDER')):
    blockchain_provider = web3.Web3.WebsocketProvider(config.get('ETH_PROVIDER'))
elif re.match(re_http, config.get('ETH_PROVIDER')):
    blockchain_provider = web3.Web3.HTTPProvider(config.get('ETH_PROVIDER'))
w3 = web3.Web3(blockchain_provider)


chain_spec = ChainSpec.from_chain_str(config.get('CIC_CHAIN_SPEC'))
CICRegistry.init(w3, config.get('CIC_REGISTRY_ADDRESS'), chain_spec)
CICRegistry.add_path(config.get('ETH_ABI_DIR'))

chain_registry = ChainRegistry(chain_spec)
CICRegistry.add_chain_registry(chain_registry, True)



api = Api(str(chain_spec))

f = open(os.path.join(config.get('ETH_ABI_DIR'), 'ERC20.json'))
erc20_abi = json.load(f)
f.close()

def get_tokens():
    tokens = []
    token_index = TokenIndex(w3, config.get('CIC_TOKEN_INDEX_ADDRESS'))
    token_count = token_index.count()
    for i in range(token_count):
        tokens.append(token_index.get_index(i))
    logg.debug('tokens {}'.format(tokens))
    return tokens

def get_addresses():
    address_index = AccountRegistry(w3, config.get('CIC_ACCOUNTS_INDEX_ADDRESS'))
    address_count = address_index.count()
    addresses = address_index.last(address_count-1)
    logg.debug('addresses {} {}'.format(address_count, addresses))
    return addresses

random.seed()

tokens = get_tokens()
addresses = get_addresses()
random_addresses = random.sample(addresses, k=len(addresses))

q_out = queue.Queue(batchsize)
q_tx = queue.Queue(batchsize)
q_in = queue.Queue(batchsize)
running = threading.Event()
running.set()


# handle graceful shutdown
run = True
def inthandler(name, frame):
    logg.warning('got {}, stopping'.format(name))
    global running
    running.clear()
signal.signal(signal.SIGTERM, inthandler)
signal.signal(signal.SIGINT, inthandler)


class Generator(threading.Thread):


    def __init__(self, ch, tokens):
        super(Generator, self).__init__()
        self.logg = logging.getLogger('generator')
        self.tokens = tokens
        self.ch = ch
   

    def get_token_with_balance(self, sender):
        return self.tokens[0]


    def run(self):
        while self.ch.is_set():
            sender = None
            recipient = None
            try:
                (sender, recipient) = q_out.get_nowait()
            except queue.Empty:
                time.sleep(0.1)
                continue
            token_address = self.get_token_with_balance(sender)

            c = w3.eth.contract(abi=erc20_abi, address=token_address)
            token_symbol = c.functions.symbol().call()
            sender_balance = c.functions.balanceOf(sender).call()
            amount = int(random.random() * (sender_balance))

            t = api.transfer(sender, recipient, amount, token_symbol)
            q_tx.put(sender)
            self.logg.info('transfer {} {} from {} to {} => {}'.format(amount, token_symbol, sender, recipient, t))


class Monitor(threading.Thread):


    def __init__(self, ch, node_uri):
        super(Monitor, self).__init__()
        self.logg = logging.getLogger('monitor')
        self.conn = websocket.create_connection(node_uri)
        o = {
                "jsonrpc": "2.0",
                "method": "eth_blockNumber",
                "id": str(uuid.uuid4()),
                "params": [],
                }
        self.conn.send(json.dumps(o).encode('utf-8'))
        r =  self.conn.recv()
        o = json.loads(r)
        self.block = int(o['result'][2:], 16) + 1
        self.logg.debug('starting at block {}'.format(self.block))
        self.addresses = {}
        self.ch = ch


    def __del__(self):
        self.conn.close()


    def __process_queue(self):
        i = 0
        while True:
            try:
                sender = q_tx.get_nowait()
                self.addresses[sender.lower()] = True
                self.logg.debug('add address {} to tx monitor'.format(sender))
                i += 1
            except queue.Empty:
                break

        if i == 0:
            logg.info('empty tx queue')


    def run(self):
        while self.ch.is_set():
            block_number = '0x' + self.block.to_bytes(8, 'big').hex()
            logg.debug('attempting to get block {}'.format(block_number))
            o = {
                "jsonrpc": "2.0",
                "method": "eth_getBlockByNumber",
                "id": str(uuid.uuid4()),
                "params": [block_number, True],
                }
            self.conn.send(json.dumps(o).encode('utf-8'))
            r = self.conn.recv()
            o = json.loads(r)
            print('block {}'.format(o))
            if o['result'] != None:
                self.block += 1
                for tx in o['result']['transactions']:
                    print('qin put tx {} from {}'.format(tx['hash'], tx['from']))
                    if self.addresses.get(tx['from']) != None:
                        del(self.addresses[tx['from']])
                        q_in.put(tx['from'])
                        
            time.sleep(5)
            self.__process_queue()
            

generator = Generator(running, tokens)
monitor = Monitor(running, 'ws://localhost:8546')


def send_one(addresses):
    sender_idx = random.randint(0, len(addresses)-1)
    sender = addresses.pop(sender_idx) 
    recipient = random.choice(addresses)
    try:
        q_out.put_nowait((sender, recipient))
        logg.debug('removing sender {} from addresslist'.format(sender))
    except queue.Full:
        logg.debug('queue full, sleeping a bit')
        time.sleep(0.5)


def main():
    monitor.start()
    generator.start()

    for i in range(batchsize):
        send_one(addresses)

    while running.is_set():
        try:
            sender = q_in.get_nowait()
            sender_cs = web3.Web3.toChecksumAddress(sender)
            addresses.append(sender_cs)
            logg.debug('replacing sender {} in addresslist'.format(sender))

            if len(addresses) > 1:
                send_one(addresses)
        except queue.Empty:
            time.sleep(0.5)

   
    logg.warning('quitting')
    generator.join()
    monitor.join()


if __name__ == '__main__':
    main()
