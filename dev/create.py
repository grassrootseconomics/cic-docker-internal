#!/usr/bin/python
#import socket
import sys
import os
import logging
import uuid
import json

import celery
from cic_eth.api import Api
import confini
import argparse
import redis

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger('create_account_script')
logging.getLogger('confini').setLevel(logging.WARNING)
logging.getLogger('gnupg').setLevel(logging.WARNING)

config_dir = os.environ.get('CONFINI_DIR', '/usr/local/etc/cic')

argparser = argparse.ArgumentParser()
argparser.add_argument('--no-register', dest='no_register', action='store_true', help='Do not register new account in on-chain accounts index')
argparser.add_argument('-i', '--chain-spec', dest='i', type=str, help='chain spec')
argparser.add_argument('--timeout', default=20.0, type=float, help='Callback timeout')
argparser.add_argument('-q', type=str, default='cic-eth', help='Task queue')
argparser.add_argument('-v', action='store_true', help='Be verbose')
argparser.add_argument('-vv', action='store_true', help='Be more verbose')
args = argparser.parse_args()

if args.vv:
    logg.setLevel(logging.DEBUG)
if args.v:
    logg.setLevel(logging.INFO)

config = confini.Config(config_dir, os.environ.get('CONFINI_ENV_PREFIX'))
config.process()
args_override = {
        'CIC_CHAIN_SPEC': getattr(args, 'i'),
        }

celery_app = celery.Celery(broker=config.get('CELERY_BROKER_URL'), backend=config.get('CELERY_RESULT_URL'))

#callback_socket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
#callback_socket.bind(('0.0.0.0', 0))
#callback_socket.settimeout(20.0)
#callback_socket.listen(1)
#callback_sockaddr = callback_socket.getsockname()
#api = Api(
#        config.get('CIC_CHAIN_SPEC'),
#        queue=args.q,
#        callback_param='{}:{}'.format(callback_sockaddr[0], callback_sockaddr[1]),
#        callback_task='cic_eth.callbacks.tcp.tcp',
#        callback_queue=args.q,
#        )

redis_host = config.get('REDIS_HOST')
redis_port = config.get('REDIS_PORT')
redis_db = config.get('REDIS_DB')
redis_channel = str(uuid.uuid4())
r = redis.Redis(redis_host, redis_port, redis_db)
ps = r.pubsub()
ps.subscribe(redis_channel)
ps.get_message()

api = Api(
        config.get('CIC_CHAIN_SPEC'),
        queue=args.q,
        #callback_param='{}:{}:{}:{}'.format(redis_host, redis_port, redis_db, redis_channel),
        callback_param='{}:{}:{}:{}'.format('redis', 6379, redis_db, redis_channel),
        callback_task='cic_eth.callbacks.redis.redis',
        callback_queue=args.q,
        )

registration_account = None
#t = api.create_account(registration_account=registration_account)
if len(sys.argv) > 1:
    registration_account = config.get('DEV_ETH_ACCOUNT_ACCOUNTS_INDEX_WRITER', None)

logg.debug('accounts index writer NOT USED {}'.format(registration_account))

register = not args.no_register
logg.debug('register {}'.format(register))
t = api.create_account(register=register)

ps.get_message()
m = ps.get_message(timeout=args.timeout)
print(json.loads(m['data']))

#c = callback_socket.accept()
#print(c.recv(4096))
#c.close()
#callback_socket.close()

